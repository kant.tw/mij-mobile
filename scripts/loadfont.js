
var font_normal = new FontFaceObserver('NotoSans', {
	weight: 'normal'
});

var font_light = new FontFaceObserver('NotoSans', {
	weight: 300
});

var font_thin = new FontFaceObserver('NotoSans', {
	weight: 100
});

var font_Bold = new FontFaceObserver('NotoSans', {
	weight: 'Bold'
});


Promise.all([font_normal.load(null, 3000), font_light.load(null, 3000), font_thin.load(null, 3000), font_Bold.load(null, 3000)]).then(function() {
	console.log('chinese fonts loaded');
	document.documentElement.className += 'NotoSans';
}).catch(function() {
	console.log('chinese fonts not loaded under 3 sec');
	document.documentElement.className += 'NotoSans';
});	
