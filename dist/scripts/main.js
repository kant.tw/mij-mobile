$(document).ready(function() {

	$('#locToggle').on('click', function(e) {
		if (!$('body').hasClass('locOpened')) {
			$('body').addClass('locOpened').removeClass('menuOpened');
		} else {
			$('body').removeClass('locOpened');
		}

	});

	$('.js-closeLoc').on('click', function(e) {
		e.preventDefault();
		$('body').removeClass('locOpened');
	});

	$('.locWrap select').on('change', function(e) {
		$(this).addClass('beenSelected');
	});


	var offCanvas = {
		init: function() {
			$('.mainNav').find('.submenu').each(function() {
				$(this).data('menuheight', $(this).height());
				$(this).height(0);
				$(this).css({opacity: 1});
			});
			this.bindEvents();
		}, 

		accTrigger: function() {
			$('.js-acchead').on('click', function(e) {
				e.preventDefault();
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).next('.submenu').removeClass('active').animate({height: 0}, 400, 'easeOutQuad');
				} else {
					var targetHeight = $(this).next('.submenu').data('menuheight');
					$(this).addClass('active');
					$(this).next('.submenu').addClass('active').animate({height: targetHeight}, 400, 'easeOutQuad');
					//close others
					var siblings = $(this).parent('li').siblings().find('.js-acchead');
					// console.log('test siblings: ' + siblings.length);
					siblings.removeClass('active');
					siblings.next('.submenu').removeClass('active').animate({height: 0}, 400, 'easeOutQuad');
				}
				
			});
		},

		toggleTrigger: function() {
			$('#menuToggle').on('click', function(e) {
				e.preventDefault();
				if ($('body').hasClass('menuOpened')) {
					$('body').removeClass('menuOpened openPageCover');
					$('.slogan').css("display", 'none');//新增

					if ($('#fullpage').length > 0) {
						$.fn.fullpage.setAllowScrolling(true);	
					}
					
				} else {
					$('body').addClass('menuOpened openPageCover').removeClass('locOpened');
					$('.slogan').css("display", 'block');//新增

					if ($('#fullpage').length > 0) {
						$.fn.fullpage.setAllowScrolling(false);
					}
				}
			});
		},

		closeTrigger: function() {
			$('.js-closeNav').on('click', function(e) {
				e.preventDefault();
				$('body').removeClass('menuOpened openPageCover');
				$('.slogan').css("display", 'none');//新增

				if ($('#fullpage').length > 0) {
					$.fn.fullpage.setAllowScrolling(true);

				}
			});
		},

		styleTrigger: function() {
			$('.submenu a').on('click', function(e) {
				$(this).toggleClass('active');
			});
		},

		bindEvents: function() {
			this.toggleTrigger();
			this.accTrigger();
			this.closeTrigger();
			this.styleTrigger();
		}
	}
	offCanvas.init();



	//center banner background image
	function CenterBannerImg(imgArray) {

		this.imgArray = imgArray;
		this.init = function() { 
			var self = this;
			self.imgArray.each(function(idx) {
				self.getImgSize($(this));
			});
		};

	}

	CenterBannerImg.prototype.getImgSize = function(img) {
		var self = this;
		var imgSrc = img.attr('src');
		// console.log('src: ' + imgSrc);
		var newImg = new Image();
		newImg.onload = function() {
			var height = newImg.height;
			var width = newImg.width;
			// console.log('img load');
			self.centerCalculate(img, width, height);
		}
		newImg.src = imgSrc;
	}

	CenterBannerImg.prototype.centerCalculate = function(img, imgWidth, imgHeight) {
		var containerWidth = $(window).width(), containerHeight = $(window).height() - 60; //要扣掉header高度
		var browserRatio = containerWidth/containerHeight;
		var srcRatio = imgWidth/imgHeight;

		if (browserRatio > srcRatio) {
			var currentImgHeight = containerWidth/srcRatio;
			img.width(containerWidth);
			img.height((containerWidth*imgHeight)/imgWidth);
			img.css({'top': -(currentImgHeight - containerHeight)/2 + 60, 'left': 0});

		} else if (browserRatio <= srcRatio) {
			// console.log('2');
			var currentImgWidth = containerHeight*srcRatio;
			img.height(containerHeight);
			img.width(currentImgWidth);
			// console.log('test currentImgWidth ' + currentImgWidth + ', containerWidth' + containerWidth);
			img.css({'left': -(currentImgWidth - containerWidth)/2, 'top': 60});
		} else {

		}
		img.css('display', 'block');
	}

	new CenterBannerImg($('.js-centerImg')).init();




	// $('body').on('click', '.js-clickFlash', function(e) {
	// 	$(this).addClass('showBlue');
	// });
});